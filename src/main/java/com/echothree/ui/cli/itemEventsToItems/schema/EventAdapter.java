// --------------------------------------------------------------------------------
// Copyright 2002-2022 Echo Three, LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// --------------------------------------------------------------------------------

package com.echothree.ui.cli.itemEventsToItems.schema;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;

public class EventAdapter
        extends BaseTypeAdapter<Event> {

    @Override
    public void write(final JsonWriter jsonWriter, final Event event)
            throws IOException {
        throw new IOException("Not Implemented");
    }

    @Override
    public Event read(final JsonReader jsonReader)
            throws IOException {
        Long eventId = null; // Required
        Long eventTime = null; // Required
        Integer eventTimeSequence = null; // Required
        String entityRef = null; // Required
        String id = null; // Required
        String eventTypeName = null; // Required
        String relatedEntityRef = null;
        String relatedEventTypeName = null;
        String createdByEntityRef = null; // Required

        jsonReader.beginObject();
        while(jsonReader.hasNext()) {
            switch(jsonReader.nextName()) {
                case "eventId" -> eventId = nextLongOrNull(jsonReader);
                case "eventTime" -> eventTime = nextLongOrNull(jsonReader);
                case "eventTimeSequence" -> eventTimeSequence = nextIntegerOrNull(jsonReader);
                case "entityRef" -> entityRef = nextStringOrNull(jsonReader);
                case "id" -> id = nextStringOrNull(jsonReader);
                case "eventTypeName" -> eventTypeName = nextStringOrNull(jsonReader);
                case "relatedEntityRef" -> relatedEntityRef = nextStringOrNull(jsonReader);
                case "relatedEventTypeName" -> relatedEventTypeName = nextStringOrNull(jsonReader);
                case "createdByEntityRef" -> createdByEntityRef = nextStringOrNull(jsonReader);
            }
        }
        jsonReader.endObject();

        return new Event(eventId, eventTime, eventTimeSequence, entityRef, id, eventTypeName, relatedEntityRef,
                relatedEventTypeName, createdByEntityRef);
    }

}
