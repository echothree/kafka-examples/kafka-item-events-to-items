// --------------------------------------------------------------------------------
// Copyright 2002-2022 Echo Three, LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// --------------------------------------------------------------------------------

package com.echothree.ui.cli.itemEventsToItems;

/**
 * Hello world!
 *
 */
public class App {

    public static final String BOOTSTRAP_SERVERS = "kafka1.echothree.com:9092,kafka2.echothree.com:9092,kafka3.echothree.com:9092";
    public static final String INCOMING_TOPIC = "echothree-item-events-json";
    public static final String OUTGOING_TOPIC = "echothree-items-json";

    public static void main(String[] args)
            throws Exception {
        new ItemEventsToItems(BOOTSTRAP_SERVERS, INCOMING_TOPIC, OUTGOING_TOPIC).run();
    }

}
